# Static Content & Laravel API Pod Proxy for Kubernetes

This image exposes port 80 and contains boilerplate configuration to reverse proxy a SPA with a Laravel backend.

It assumes that the following services are exposed in the pod:

## Laravel or Equivalent PHP-FPM Service [9000]

An application handling requests on any path beginning with `/api/`.

This configuration expects the service to be based on the [Web Artisans Laravel FPM](https://bitbucket.org/web-artisans/laravel-fpm) Docker image.

## Static Site or Equivalent Service [8080]

A service serving static content on any path *except* `/api/`. Typically this will be a HTTP server serving a single page application of some kind which consumes the Laravel API.

We frequently deploy a Node JS application which compiles static assets with configuration sourced from Kubernetes as this component of the pod.